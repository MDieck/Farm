package de.bioinf.giessen.mdieck.advjava.farm.animal;

import de.bioinf.giessen.mdieck.advjava.farm.objects.StrawBundle;

/**
 * Created by marius on 08.05.16.
 */
public interface GrassEater extends Animal{

    /**
     * Eats the straw handed over in the Strawbundle and will be eaten completely.
     * @param strawBundle strawbundle to eat
     * @post increases the hunger index of the animal by the size of the strawBundle
     */
    void eat(StrawBundle strawBundle);
}
