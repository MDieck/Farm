package de.bioinf.giessen.mdieck.advjava.farm.animal;

/**
 * Created by marius on 08.05.16.
 */
interface Shearable {
    /**
     * Shears the animal and returns the amount of wool produced
     * @return amount of sheared wool
     */
    int shear();
}
