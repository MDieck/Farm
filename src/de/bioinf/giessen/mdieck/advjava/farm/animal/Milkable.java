package de.bioinf.giessen.mdieck.advjava.farm.animal;

/**
 * Created by marius on 08.05.16.
 */
interface Milkable {
    /**
     * Milks the animal and returns the amount of produced milk
     * @return amount of gathered milk
     */
    int milk();
}
