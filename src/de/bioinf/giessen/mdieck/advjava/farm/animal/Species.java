package de.bioinf.giessen.mdieck.advjava.farm.animal;

/**
 * Created by marius on 08.05.16.
 */
public enum Species {
    Goat, Cow, Sheep
}