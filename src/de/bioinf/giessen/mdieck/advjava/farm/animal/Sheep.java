package de.bioinf.giessen.mdieck.advjava.farm.animal;

import de.bioinf.giessen.mdieck.advjava.farm.objects.StrawBundle;

import java.util.Random;

/**
 * Created by marius on 08.05.16.
 */
public class Sheep implements GrassEater, Shearable {
    int hunger;
    final Species species = Species.Sheep;
    Random rng = new Random();
    String name;

    /**
     * Creates a sheep with a random hunger value
     */
    public Sheep(String name){
        this.hunger = rng.nextInt(100);
        this.name = name;
    }

    @Override
    public int gatherRessources() {
        return shear();
    }

    @Override
    public void eat(StrawBundle strawBundle) {
        int stackHeight = strawBundle.getStackHeight();
        if (stackHeight > 0){
            strawBundle.consume(stackHeight);
            this.hunger = this.hunger + stackHeight;
        }
    }

    @Override
    public int shear() {
        return 10;
    }

    @Override
    public int hunger() {
        return hunger;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString(){
        return this.name;
    }
}