package de.bioinf.giessen.mdieck.advjava.farm.animal;

/**
 * Created by marius on 08.05.16.
 */

public interface Animal {

    /**
     * Returns the hunger value of the current animal
     * @return hunger value
     */
    int hunger();

    /**
     * Gets the collectable ressources of the species
     * @return amount of gathered ressources
     */
    int gatherRessources();

    /**
     * Returns the species of the animal
     * @return Species of the enum
     */
    Species getSpecies();

    String getName();

    @Override
    String toString();
}
