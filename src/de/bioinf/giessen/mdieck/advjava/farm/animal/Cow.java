package de.bioinf.giessen.mdieck.advjava.farm.animal;

import de.bioinf.giessen.mdieck.advjava.farm.objects.StrawBundle;

import java.util.Random;

/**
 * Created by marius on 08.05.16.
 */
public class Cow implements GrassEater, Milkable {
    int hunger;
    final Species species = Species.Cow;
    Random rng = new Random();
    String name;

    /**
     * Creates a Cow with a random hunger value
     */
    public Cow(String name){
        this.hunger = rng.nextInt(100);
        this.name = name;
    }

    @Override
    public int gatherRessources() {
        return this.milk();
    }

    @Override
    public void eat(StrawBundle strawBundle) {
        int stackHeight = strawBundle.getStackHeight();
        if (stackHeight > 0){
            strawBundle.consume(stackHeight);
            this.hunger = this.hunger + stackHeight;
        }
    }

    @Override
    public int hunger() {
        return this.hunger;
    }

    @Override
    public int milk() {
        return 10;
    }

    public Species getSpecies() {
        return species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString(){
        return this.name;
    }
}