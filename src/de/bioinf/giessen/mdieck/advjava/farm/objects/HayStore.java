package de.bioinf.giessen.mdieck.advjava.farm.objects;

/**
 * Created by marius on 08.05.16.
 */
public class HayStore {
    int stackSize;

    /**
     * creates a hay store, gets the initial amount if
     * @param stackSize initial hay
     */
    public HayStore(int stackSize){
        this.stackSize = stackSize;
    }

    /**
     * Creates a straw bundle of the size handed over if enough hay is available in the store
     * @param amount size of the hay stack
     * @return StrawBundle of the requested size if possible
     * @post reduces the size of the hay stack according to the size of the returned StrawBundle
     */
    public StrawBundle getStrawBundle(int amount){
        if(amount <= this.stackSize){
            this.stackSize = this.stackSize - amount;
            return new StrawBundle(amount);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Adds more hay to the store
     * @param amount amount of additional hay
     * @post increases the amount of hay in the haystore by the given number
     */
    public void storeHay(int amount) {
        this.stackSize = this.stackSize + amount;
    }

    /**
     * Returns the amount of stored hay
     * @return amount of stored hay
     */
    public int getStackSize() {
        return stackSize;
    }
}
