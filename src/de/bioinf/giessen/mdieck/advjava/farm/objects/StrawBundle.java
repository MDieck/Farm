package de.bioinf.giessen.mdieck.advjava.farm.objects;

/**
 * Created by marius on 08.05.16.
 */
public class StrawBundle {

    int amount;

    public StrawBundle(int amount){
        this.amount = amount;
    }

    /**
     * Consumes the given amount of the hay if the straw bundle
     * @param amount amount of hay to consume
     * @return if the amount of hay could be consumed
     */
    public boolean consume(int amount){
        if (this.amount >= amount){
            this.amount = this.amount - amount;
            return true;
        } else {
            return false;
        }
    }


    /**
     * Amount of remaining straw in the bundle
     * @return remaining straw
     */
    public int getStackHeight(){
        return this.amount;
    }


}
