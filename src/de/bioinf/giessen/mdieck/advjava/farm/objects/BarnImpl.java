package de.bioinf.giessen.mdieck.advjava.farm.objects;

import de.bioinf.giessen.mdieck.advjava.farm.animal.Animal;
import de.bioinf.giessen.mdieck.advjava.farm.animal.GrassEater;
import de.bioinf.giessen.mdieck.advjava.farm.animal.Species;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by marius on 08.05.16.
 */
public class BarnImpl{
    HayStore hayStore;
    Map<Species, List<Animal>> animalLists;

    private int cowMilkStore = 0;
    private int goatMilkStore = 0;
    private int sheepWool = 0;


    /**
     * Creates a barn that stores animals
     */
    public BarnImpl(){
        hayStore = new HayStore(1000);
        List cowList = new ArrayList<>();
        List sheepList = new ArrayList<>();
        List goatList = new ArrayList<>();
        animalLists = new HashMap<>();

        animalLists.put(Species.Cow, cowList);
        animalLists.put(Species.Sheep, sheepList);
        animalLists.put(Species.Goat, goatList);

    }

    /**
     * Addas the provided animal to the barn
     * @param animal
     */
    public void addAnimal(Animal animal) {
        animalLists.get(animal.getSpecies()).add(animal);
    }

    /**
     * Collects all ressources from the animals in the barn
     */
    public void treatAll() {
        treatCows();
        treatGoats();
        treatSheeps();
    }

    private void treatCows() {
        for (Animal animal : animalLists.get(Species.Cow)){
            this.cowMilkStore = this.cowMilkStore + animal.gatherRessources();
        }
    }

    private void treatGoats() {
        for (Animal animal : animalLists.get(Species.Goat)){
            this.goatMilkStore = this.goatMilkStore + animal.gatherRessources();
        }
    }

    private void treatSheeps() {
        for (Animal animal : animalLists.get(Species.Sheep)){
            this.sheepWool = this.sheepWool + animal.gatherRessources();
        }
    }

    /**
     * Feeds all animals with the same amount of hay
     * @param amount amount of hay feeded
     * @post increases hunger value for all animals
     */
    public void feedAll(int amount) {
        for (Species species : animalLists.keySet()){
            for(Animal animal : animalLists.get(species)){
                GrassEater eater =  (GrassEater) animal;
                eater.eat(hayStore.getStrawBundle(amount));
            }
        }
    }

    /**
     * Amount of stored cow milk
     * @return stored cow milk
     */
    public int getCowMilkStore() {
        return cowMilkStore;
    }

    /**
     * Amount of stored goat milk
     * @return stored goat milk
     */
    public int getGoatMilkStore() {
        return goatMilkStore;
    }

    /**
     * Amount of stored wool
     * @return stored wool
     */
    public int getSheepWool() {
        return sheepWool;
    }

    /**
     * Amount of hay in the haystore of the barn
     * @return hay in store
     */
    public int getHayAmount(){
        return hayStore.getStackSize();
    }
}
