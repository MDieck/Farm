package de.bioinf.giessen.mdieck.advjava.farm;

import de.bioinf.giessen.mdieck.advjava.farm.animal.Cow;
import de.bioinf.giessen.mdieck.advjava.farm.animal.Goat;
import de.bioinf.giessen.mdieck.advjava.farm.animal.Sheep;
import de.bioinf.giessen.mdieck.advjava.farm.objects.BarnImpl;

/**
 * Created by marius on 08.05.16.
 */
public class run {
    public static void main(String[] args){
        BarnImpl barn = new BarnImpl();
        barn.addAnimal(new Cow("Susi"));
        barn.addAnimal(new Sheep("Meck"));
        barn.addAnimal(new Goat("Häck"));
        barn.addAnimal(new Cow("Hok"));
	//sjdyhfish


        barn.feedAll(50);
        barn.treatAll();
    }
}
