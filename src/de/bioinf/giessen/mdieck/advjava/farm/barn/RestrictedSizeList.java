package de.bioinf.giessen.mdieck.advjava.farm.barn;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by marius on 29.05.16.
 */
public interface RestrictedSizeList<T> extends Iterable<T>, Collection<T>{

    T get (int i);
    boolean isEmtpy();
    int getMaxSize();
    void setMaxSize(int maxSize);
}
