package de.bioinf.giessen.mdieck.advjava.farm.barn;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * Created by marius on 29.05.16.
 */
public class RestrictedSizeListImpl<T> implements RestrictedSizeList<T> {

    private T[] objectStore;
    private int storedObjects = 0;
    private int maxSize;
    private int currentObjectNumber = 0;

    public RestrictedSizeListImpl(int maxSize){
        this.objectStore = (T[]) new Object[maxSize];
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(T item){
        if (maxSize > storedObjects){
            this.objectStore[storedObjects] = item;
            this.storedObjects++;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public T get(int i) throws IllegalArgumentException{
        if (i < this.storedObjects){
            return this.objectStore[i];
        } else throw new IllegalArgumentException();
    }

    @Override
    public boolean isEmtpy() {
        if(this.storedObjects > 0){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getMaxSize() {
        return this.maxSize;
    }

    @Override
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public int size() {
        return this.storedObjects;
    }

    @Override
    public boolean isEmpty() {
        if (this.storedObjects > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < this.storedObjects; i++) {
            if (this.objectStore.equals(o)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> it = new MyIterable<T>(this.objectStore);
        return it;

    }

    @Override
    public T[] toArray() {
        return this.objectStore;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) this.objectStore;
    }

    public void clear() {
        for (int i = 0; i < this.objectStore.length; i++) {
            this.objectStore[i] = null;
        }
    }


    class MyIterable<T> implements Iterator<T> {

        private T[] elements = null;
        private int currentIndex = 0;

        MyIterable(T[] elements) {
            this.elements = elements;
        }

        @Override
        public boolean hasNext() {
            if (this.currentIndex < storedObjects) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public T next() {
            T returnObject = elements[this.currentIndex];
            this.currentIndex++;
            return returnObject;
        }
    }


}
