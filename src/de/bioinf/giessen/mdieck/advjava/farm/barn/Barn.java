package de.bioinf.giessen.mdieck.advjava.farm.barn;

import de.bioinf.giessen.mdieck.advjava.farm.animal.Animal;
import de.bioinf.giessen.mdieck.advjava.farm.animal.Cow;

import java.util.List;

/**
 * Created by marius on 29.05.16.
 */
public interface Barn<T extends Animal> extends RestrictedSizeList<T>{
}
