package de.bioinf.giessen.mdieck.advjava.farm.barn;


import de.bioinf.giessen.mdieck.advjava.farm.animal.Cow;
import de.bioinf.giessen.mdieck.advjava.farm.animal.Goat;

import java.util.Iterator;

/**
 * Created by marius on 29.05.16.
 */
public class Test {
    public static void main(String [] args){
        Barn<Cow> cowBarn = new BarnImpl<>(5);
        cowBarn.add(new Cow("Emma"));
        cowBarn.add(new Cow("Susi"));
        cowBarn.add(new Cow("Laura"));

        Barn<Goat> goatBarn = new BarnImpl<>(3);
        goatBarn.add(new Goat("Meck"));
        goatBarn.add(new Goat("Muck"));


        System.out.println("Kühe im Kuhstall:");
        for (Cow cow : cowBarn) {
            System.out.println(cow);
        }

        System.out.println("");
        System.out.println("Ziegen im Ziegenstall:");
        for (Goat goat : goatBarn) {
            System.out.println(goat);
        }
    }
}
