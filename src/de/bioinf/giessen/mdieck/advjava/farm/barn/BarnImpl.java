package de.bioinf.giessen.mdieck.advjava.farm.barn;

import de.bioinf.giessen.mdieck.advjava.farm.animal.Animal;
import de.bioinf.giessen.mdieck.advjava.farm.animal.Cow;

/**
 * Created by Marius on 05.06.2016.
 */
public class BarnImpl<T extends Animal> extends RestrictedSizeListImpl<T> implements Barn<T> {

    public BarnImpl(int maxSize) {
        super(maxSize);
    }
}
